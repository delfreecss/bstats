
   </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
</div>

    <!-- /.wrap -->

<div style="clear:both;"></div>

  <!-- Footer -->
        <div id="footer">
              <div class="container">
                <p class="text-muted credit">&copy; 2017. All rights reserved.</p>
              </div>
        </div>



    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- CKEDITOR-->
    <script src="ckeditor/ckeditor.js"></script>
    <!-- CKFINDER-->
    <script src="ckfinder/ckfinder.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

     <script>
            CKEDITOR.replace( 'content', {
                filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
                filebrowserUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
            } );


    </script>

  </body>
</html>
