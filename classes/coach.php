<?php

/**
 * Class to handle coaches
 */

class Coach
{

  // Properties

  /**
  * @var int The coach ID
  */
  public $id = null;

   /**
  * @var string first name of the coach
  */
  public $fname = null;

  /**
 * @var string middle name of the coach
 */
 public $mname = null;

   /**
  * @var string last name of the coach
  */
  public $lname = null;


  /**
  * @var string address of the coach
  */
  public $address = null;

  /**
  * @var string contact  number of the coach
  */
  public $contactNo = null;
  /**
  * @var int When the article was published
  */
  public $bDate = null;


  /**
  * @var string The filename extension of the coach image full-size and thumbnail images (empty string means the coach has no image)
  */
  public $imageExtension = "";

  /**
  * @var int status of coach
  */
  public $status = null;

  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */

  public function __construct( $data=array() ) {
    if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];

    /**
    *It's good security practice to filter data on input like this, only allowing acceptable values and characters through.
    */

    if ( isset( $data['fname'] ) ) $this->fname = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['fname'] );
    if ( isset( $data['mname'] ) ) $this->mname = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['mname'] );
    if ( isset( $data['lname'] ) ) $this->lname = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['lname'] );
    if ( isset( $data['address'] ) ) $this->address = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['address'] );
    if ( isset( $data['contactNo'] ) ) $this->contactNo = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['contactNo'] );

    if ( isset( $data['bDate'] ) ) $this->bDate =  $data['bDate'];

    if ( isset( $data['imageExtension'] ) ) $this->imageExtension = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\$ a-zA-Z0-9()]/", "", $data['imageExtension'] );
    if ( isset( $data['status'] ) ) $this->status =  $data['status'];

  }


  /**
  * Sets the object's properties using the edit form post values in the supplied array
  *
  * @param assoc The form post values
  */

  public function storeFormValues ( $params ) { //this function will handle the create and update Coach

    // Store all the parameters
    $this->__construct( $params );

  }


  /**
  * Returns an Coach object matching the given coach ID
  *
  * @param int TheCoach ID
  * @return Coach|false The coach object, or false if the record was not found or there was a problem
  */

  //static keyword to the method definitionallows the method to be called directly without specifying an object

  public static function getById( $id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD ); // makes a connection to the MySQL database using the login details from the config.php
    $sql = "SELECT * FROM coaches WHERE id = :id";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":id", $id, PDO::PARAM_INT );
    $st->execute();
    $row = $st->fetch();
    $conn = null; //close connection
    if ( $row ) return new Coach( $row );
  }


   /**
  * Returns all (or a range of) Coach Item objects in the DB
  *
  * @param int Optional The number of rows to return (default=all)
  * @param int Optional Return just articles in the category with this ID
  * @param string Optional column by which to order the articles (default="name ASC")
  * @return Array|false A two-element array : results => array, a list of Article objects; totalRows => Total number of menu items
  */

  public static function getList( $numRows=0, $perPage=10000000, $order="lname ASC" ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM coaches
            ORDER BY " . $order  . " LIMIT :numRows, :perPage";

    $st = $conn->prepare( $sql );
    $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
    $st->bindValue( ":perPage", $perPage, PDO::PARAM_INT );
    $st->execute();
    $list = array();

    while ( $row = $st->fetch() ) {
      $coach = new Coach( $row );
      $list[] = $coach;
    }

    // Now get the total number of coaches that matched the criteria
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }


  /**
  * Inserts the current Coach object into the database, and sets its ID property.
  */

  public function insert() {

    // Does the Coach object already have an ID?
    if ( !is_null( $this->id ) ) trigger_error ( "Coach::insert(): Attempt to insert an Coach object that already has its ID property set (to $this->id).", E_USER_ERROR );



    // Insert the Coach
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO coaches (fname,mname,lname,address,contactNo,bDate,imageExtension,status) VALUES (:fname, :mname, :lname, :address, :contactNo, :bDate,:imageExtension,:status)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":fname", $this->fname, PDO::PARAM_STR );
    $st->bindValue( ":mname", $this->mname, PDO::PARAM_STR );
    $st->bindValue( ":lname", $this->lname, PDO::PARAM_STR );
    $st->bindValue( ":address", $this->address, PDO::PARAM_STR );
    $st->bindValue( ":contactNo", $this->contactNo, PDO::PARAM_STR );
    $st->bindValue( ":bDate", $this->bDate,  PDO::PARAM_STR  );
    $st->bindValue( ":imageExtension",$this->imageExtension, PDO::PARAM_STR );
      $st->bindValue( ":status",$this->imageExtension, PDO::PARAM_INT );
    $st->execute();
    $this->id = $conn->lastInsertId();
    $conn = null;
  }


  /**
  * Updates the current Article object in the database.
  */

  public function update() {

    // Does the Coach object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "Coach::update(): Attempt to update an Coach object that does not have its ID property set.", E_USER_ERROR );

    // Update the Coach
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "UPDATE coaches SET fname=:fname,mname=:mname,lname=:lname,address=:address,contactNo=:contactNo,bDate=:bDate,imageExtension=:imageExtension,status=:status WHERE id = :id";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":fname", $this->fname, PDO::PARAM_STR );
    $st->bindValue( ":mname", $this->mname, PDO::PARAM_STR );
    $st->bindValue( ":lname", $this->lname, PDO::PARAM_STR );
    $st->bindValue( ":address", $this->address, PDO::PARAM_STR );
    $st->bindValue( ":contactNo", $this->contactNo, PDO::PARAM_STR );
    $st->bindValue( ":bDate", $this->bDate, PDO::PARAM_STR );
    $st->bindValue( ":imageExtension",$this->imageExtension, PDO::PARAM_STR );
    $st->bindValue( ":status",$this->imageExtension, PDO::PARAM_INT );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }


  /**
  * Deletes the current Article object from the database.
  */

  public function delete() {

    // Does the Article object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "Coach::delete(): Attempt to delete Coach object that does not have its ID property set.", E_USER_ERROR );

    // Delete the MenuItem
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $st = $conn->prepare ( "DELETE FROM coaches WHERE id = :id LIMIT 1" );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }


   /**
  * Stores any image uploaded from the edit form
  *
  * @param assoc The 'image' element from the $_FILES array containing the file upload data
  */

  public function storeUploadedImage( $image ) {

    if ( $image['error'] == UPLOAD_ERR_OK )
    {
      // Does the Coach object have an ID?
      if ( is_null( $this->id ) ) trigger_error( "Coach::storeUploadedImage(): Attempt to upload an image for an Coach object that does not have its ID property set.", E_USER_ERROR );

      // Delete any previous image(s) for this coach
      $this->deleteImages();

      // Get and store the image filename extension
      $this->imageExtension = strtolower( strrchr( $image['name'], '.' ) );

      // Store the image

      $tempFilename = trim( $image['tmp_name'] );

        // die(var_dump($tempFilename));

      if ( is_uploaded_file ( $tempFilename ) ) {
        if ( !( move_uploaded_file( $tempFilename, $this->getImagePath() ) ) ) trigger_error( "Coach::storeUploadedImage(): Couldn't move uploaded file.", E_USER_ERROR );
        if ( !( chmod( $this->getImagePath(), 0666 ) ) ) trigger_error( "Coach::storeUploadedImage(): Couldn't set permissions on uploaded file.", E_USER_ERROR );
      }

      // Get the image size and type
      $attrs = getimagesize ( $this->getImagePath() );
      $imageWidth = $attrs[0];
      $imageHeight = $attrs[1];
      $imageType = $attrs[2];

      // Load the image into memory
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          $imageResource = imagecreatefromgif ( $this->getImagePath() );
          break;
        case IMAGETYPE_JPEG:
          $imageResource = imagecreatefromjpeg ( $this->getImagePath() );
          break;
        case IMAGETYPE_PNG:
          $imageResource = imagecreatefrompng ( $this->getImagePath() );
          break;
        default:
          trigger_error ( "Coach::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }

      // Copy and resize the image to create the thumbnail
      $thumbHeight = intval ( $imageHeight / $imageWidth * COACH_THUMB_WIDTH );
      $thumbResource = imagecreatetruecolor ( COACH_THUMB_WIDTH, $thumbHeight );
      imagecopyresampled( $thumbResource, $imageResource, 0, 0, 0, 0, COACH_THUMB_WIDTH, $thumbHeight, $imageWidth, $imageHeight );

      // Save the thumbnail
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          imagegif ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ) );
          break;
        case IMAGETYPE_JPEG:
          imagejpeg ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ), JPEG_QUALITY );
          break;
        case IMAGETYPE_PNG:
          imagepng ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ) );
          break;
        default:
          trigger_error ( "Coach::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }

      $this->update();
    }
  }


  /**
  * Deletes any images and/or thumbnails associated with the coach
  */

  public function deleteImages() {

    // Delete all fullsize images for this items
    foreach (glob( "../" .COACH_IMAGE_PATH . "/" . IMG_TYPE_FULLSIZE . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "Coach::deleteImages(): Couldn't delete image file.", E_USER_ERROR );
    }

    // Delete all thumbnail images for this coach
    foreach (glob( "../" . COACH_IMAGE_PATH . "/" . IMG_TYPE_THUMB . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "COACH::deleteImages(): Couldn't delete thumbnail file.", E_USER_ERROR );
    }

    // Remove the image filename extension from the object
    $this->imageExtension = "";
  }


  /**
  * Returns the relative path to the article's full-size or thumbnail image
  *
  * @param string The type of image path to retrieve (IMG_TYPE_FULLSIZE or IMG_TYPE_THUMB). Defaults to IMG_TYPE_FULLSIZE.
  * @return string|false The image's path, or false if an image hasn't been uploaded
  */

  public function getImagePath( $type=IMG_TYPE_FULLSIZE ) {
    return ( $this->id && $this->imageExtension ) ? ( "../" . COACH_IMAGE_PATH . "/$type/" . $this->id . $this->imageExtension ) : false;
  }



}

?>
