<?php include "/../../include/header.php" ?>
<?php include "/../../include/admin.php" ?>





 <h1>All Coaches</h1><hr>
 <?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="alert alert-danger">
                      <i class="glyphicon glyphicon-remove-sign"></i> &nbsp;<?php echo $results['errorMessage'] ?>
                 </div>
<?php } ?>
<?php if ( isset( $results['statusMessage'] ) ) { ?>
        <div class="alert alert-info" id="alert-info">
                      <i class="glyphicon glyphicon-thumbs-up"></i> &nbsp;<?php echo $results['statusMessage'] ?>
                 </div>
<?php } ?>

<table id="coach-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
                <th>Action</th>

            </tr>
        </thead>
</table>

<div align = "right"><hr>
  <a class="btn btn-default" href="index.php?action=newCoach"><span class="glyphicon glyphicon-plus"></span> New Coach </a>
</div>




</div>
</div>



<?php include "/../../include/footer.php" ?>


<!--########################### Alert-Info Hide and Fade #############################-->

<script type="text/javascript">
  $(document).ready (function(){

            $("#alert-info").hide();
            $("#alert-info").alert();
            $("#alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert-info").slideUp(500);
            });

  });
</script>



<!--########################### AJAX REQUEST DATA TABLE FOR COACHES LIST #############################-->
                                  <!--for  api/coach_lists.php -->


<script type="text/javascript" language="javascript" >
    $(document).ready(function() {

        var host = $(location).attr('hostname');
        var path = $(location).attr('pathname');
        path = path.substring(0, path.lastIndexOf('/'));

        // alert("http://" + host + path + "/api/coach_lists.php");

        var dataTable = $('#coach-grid').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"http://" + host + path + "/api/coach_lists.php", // json datasource   ,
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".coach-grid-error").html("");
                    $("#coach-grid").append('<tbody class="coach-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#coach-grid_processing").css("display","none");

                }
            }
        } );
    } );
</script>
