<?php include "/../../include/header.php" ?>
<?php include "/../../include/admin.php" ?>


<script>

// Prevents file upload hangs in Mac Safari
// Inspired by http://airbladesoftware.com/notes/note-to-self-prevent-uploads-hanging-in-safari

function closeKeepAlive() {
 if ( /AppleWebKit|MSIE/.test( navigator.userAgent) ) {
   var xhr = new XMLHttpRequest();
   xhr.open( "GET", "/ping/close", false );
   xhr.send();
 }
}

</script>

      <h1><?php echo $results['pageTitle']?></h1>
      <hr>
      <form action="index.php?action=<?php echo $results['formAction']?>"  method="post" enctype="multipart/form-data" onsubmit="closeKeepAlive()">


        <input type="hidden" name="coachId" value="<?php echo $results['coach']->id ?>"/>

        <?php if ( isset( $results['errorMessage'] ) ) { ?>
                <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
        <?php } ?>




      <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
              <?php if ( $results['coach'] && $imagePath = $results['coach']->getImagePath() ) { ?>

                <center> <img class="img-circle" id="itemImage" src="<?php echo $imagePath ?>" alt="Coach Image" width ="150" height = "150"/>

             <div class="form-group ">
                 <input type="checkbox" name="deleteImage" id="deleteImage" value="yes"/ > <label for="deleteImage">Delete</label>

               </div>
         </div>

           <?php } else{?>
                         <div class="form-group ">
                     <center><img class="img-circle" id="itemImage" src="http://placehold.it/150?text=NULL" alt="Coach Image" width ="150" height = "150"/>
                     </div>
          <?php } ?>
           <div class="form-group ">
                  <div class="row " >
                     <div class="col-lg-6 col-lg-offset-3 selectContainer ">
                 <input type="file" name="image" id="imageInput" placeholder="Choose an image to upload" maxlength="255" />

               </div>


                  </div>
                 </div>

            </div>
        </div>
      </div>

      <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <label class="control-label">First Name</label>
                <input class="form-control" type="text" name="fname" id="fname" placeholder="First Name of the Coach" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['coach']->fname )?>" />
            </div>

        </div>
      </div>


      <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <label class="control-label">Middle Name</label>
                <input class="form-control" type="text" name="mname" id="mname" placeholder="Middle Name of the Coach" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['coach']->mname )?>" />
            </div>
        </div>
      </div>

      <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <label class="control-label">Last Name</label>
                <input class="form-control" type="text" name="lname" id="lname" placeholder="Last Name of the Coach" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['coach']->lname )?>" />
            </div>
        </div>
      </div>

      <div class="form-group">
       <div class="row">
          <div class="col-xs-3 selectContainer">
          <label class="control-label">Contact No  </label>

          <input class="form-control" type="text" name="contactNo" id="contactNo" placeholder="Contact No" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['coach']->contactNo )?>" />
        </div>

        <div class="col-xs-3 selectContainer">
             <label class="control-label">Birth Date</label>
                <div class='input-group date'>
                  <input  class ="form-control" type="date" name="bDate" id="bDate" placeholder="YYYY-MM-DD" required maxlength="10" value="<?php echo $results['coach']->bDate ? $results['coach']->bDate  : "" ?>" />
            <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
              </div>

        </div>

       </div>
      </div>


            <div class="form-group">
             <div class="row">
                <div class="col-xs-6 selectContainer">
                <label class="control-label">Address  </label>

                <textarea class="form-control" name="address" id="address" placeholder="Address" required maxlength="1000" style="height: 5em;"><?php echo htmlspecialchars( $results['coach']->address )?></textarea>
             </div>
             </div>
            </div>




        <div class="col-xs-6 selectContainer" align = "right" class="buttons" >
          <input  class ="btn btn-primary" type="submit" name="saveChanges" value="Save Changes" />
          <input class ="btn btn-danger" type="submit" formnovalidate name="cancel" value="Cancel" />
        </div>
        <br>
      </form>
      <br><br>


<?php include "/../../include/footer.php" ?>



<!--########################### SCRIPT FOR IMAGE PREVIEW #############################-->

<script type="text/javascript">

document.getElementById("imageInput").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("itemImage").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};

</script>
