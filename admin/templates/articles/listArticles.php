<?php include "/../../include/header.php" ?>
<?php include "/../../include/admin.php" ?>





 <h1>All Articles</h1>
 <?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="alert alert-danger">
                      <i class="glyphicon glyphicon-remove-sign"></i> &nbsp;<?php echo $results['errorMessage'] ?>
                 </div>
<?php } ?>
<?php if ( isset( $results['statusMessage'] ) ) { ?>
        <div class="alert alert-info" id="alert-info">
                      <i class="glyphicon glyphicon-thumbs-up"></i> &nbsp;<?php echo $results['statusMessage'] ?>
                 </div>
<?php } ?>
 <table id="mytable" class="table table-bordred table-striped">

           <thead>
             <th>Publication Date</th>
             <th>Article</th>
             <th>Category</th>
             <th>Action</th>
           </thead>
           <tbody>
    <?php foreach ( $results['articles'] as $article ) { ?>
          <tr>

        <td><?php echo date('j M Y', $article->publicationDate)?></td>
        <td><?php echo $article->title?></td>
         <td>
            <?php echo $results['categories'][$article->categoryId]->name?>
          </td>
        <td>
        <a class="btn btn-primary btn-xs" href="index.php?action=editArticle&amp;articleId=<?php echo $article->id?>"><span class="glyphicon glyphicon-pencil"></span></a>
        <a class="btn btn-danger btn-xs" href="index.php?action=deleteArticle&amp;articleId=<?php echo $article->id?>"><span class="glyphicon glyphicon-trash"></span></a>


        </tr>

  </tbody>
    <?php } ?>
     <p style="text-align: right">
        <span class="glyphicon glyphicon-list"></span>
                      <?php echo $results['totalRows']?> article<?php echo ( $results['totalRows'] != 1 ) ? 's' : '' ?> in total.
                 </p>

</table>
<div align = "right">
  <a class="btn btn-default" href="index.php?action=newArticle"><span class="glyphicon glyphicon-plus"></span> New Article </a>
</div>

<?php
// paging buttons will be here
         include_once 'pagingArticles.php';

?>


</div>
</div>



<?php include "/../../include/footer.php" ?>


<!--########################### Alert-Info Hide and Fade #############################-->

<script type="text/javascript">
  $(document).ready (function(){

            $("#alert-info").hide();
            $("#alert-info").alert();
            $("#alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert-info").slideUp(500);
            });

  });
</script>
