 <?php

require( "../config.php" );
session_start();
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$username = isset( $_SESSION['username'] ) ? $_SESSION['username'] : "";

if (isset( $_SESSION['username'] ) && $action == 'login') {
   listArticles();
  exit;
}

if ( $action != "login" && $action != "logout" && !$username ) {
  login();
  exit;
}

switch ( $action ) {
  case 'login':
    login();
    break;
  case 'logout':
    logout();
    break;
  case 'newArticle':
    newArticle();
    break;
  case 'editArticle':
    editArticle();
    break;
  case 'deleteArticle':
    deleteArticle();
    break;
  case 'listCategories':
    listCategories();
    break;
  case 'newCategory':
    newCategory();
    break;
  case 'editCategory':
    editCategory();
    break;
  case 'deleteCategory':
    deleteCategory();
    break;

  case 'listCoaches':
    listCoaches();
    break;
  case 'newCoach':
    newCoach();
    break;
  case 'editCoach':
    editCoach();
    break;
  case 'deleteCoach':
    deleteCoach();
    break;

  default:
    listArticles();
}


function login() {

  $results = array();
  $results['pageTitle'] = "Admin Login Panel";

  if ( isset( $_POST['login'] ) ) {

    // User has posted the login form: attempt to log the user in

    if ( $_POST['username'] == ADMIN_USERNAME && sha1($_POST['password']) == ADMIN_PASSWORD ) {

      // Login successful: Create a session and redirect to the admin homepage
      $_SESSION['username'] = ADMIN_USERNAME;
      header( "Location: index.php" );

    } else {

      // Login failed: display an error message to the user
      $results['errorMessage'] = "Incorrect username or password. Please try again.";
      require( TEMPLATE_PATH . "/loginForm.php" );
    }

  } else {

    // User has not posted the login form yet: display the form
    require( TEMPLATE_PATH . "/loginForm.php" );
  }

}


function logout() {
  unset( $_SESSION['username'] );
  header( "Location: index.php" );
}


function newArticle() {

  $results = array();
  $results['pageTitle'] = "New Article";
  $results['page'] = "articles";
  $results['formAction'] = "newArticle";

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the article edit form: save the new article
    $article = new Article;
    $article->storeFormValues( $_POST );
    $article->insert();
    header( "Location: index.php?status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the article list
    header( "Location: index.php" );
  } else {

    // User has not posted the article edit form yet: display the form
    $results['article'] = new Article;
    $data = Category::getList();
    $results['categories'] = $data['results'];
    require( TEMPLATE_PATH . "/articles/editArticle.php" );
  }

}




// =========================== ARTICLES ==========================================

function editArticle() {

  $results = array();
  $results['page'] = "articles";
  $results['pageTitle'] = "Edit Article";
  $results['formAction'] = "editArticle";

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the article edit form: save the article changes

    if ( !$article = Article::getById( (int)$_POST['articleId'] ) ) {
      header( "Location: index.php?error=articleNotFound" );
      return;
    }

    $article->storeFormValues( $_POST );
    $article->update();
    header( "Location: index.php?status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the article list
    header( "Location: index.php" );
  } else {

    // User has not posted the article edit form yet: display the form
    $results['article'] = Article::getById( (int)$_GET['articleId'] );
    $data = Category::getList();
    $results['categories'] = $data['results'];
    require( TEMPLATE_PATH . "/articles/editArticle.php" );
  }

}


function deleteArticle() {

  if ( !$article = Article::getById( (int)$_GET['articleId'] ) ) {
    header( "Location: index.php?error=articleNotFound" );
    return;
  }

  $article->delete();
  header( "Location: index.php?status=articleDeleted" );
}


function listArticles() {


  $record_per_page = ARTICLES_PAGE_PER_TABLE;
  if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
  $start_from = ($page-1) * $record_per_page;


  $results = array();
  $results['page'] = "articles";
  $data = Article::getList($start_from,$record_per_page);
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $data = Category::getList();
  $results['categories'] = array();
  foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category;
  $results['pageTitle'] = "All Articles";

  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "articleNotFound" ) $results['errorMessage'] = "Error: Article not found.";
  }

  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "articleDeleted" ) $results['statusMessage'] = "Article deleted.";
  }

  require( TEMPLATE_PATH . "/articles/listArticles.php" );
}



// =========================== CATEGORIES ==========================================


function listCategories() {


  $record_per_page = CATEGORIES_PAGE_PER_TABLE;
  if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
  $start_from = ($page-1) * $record_per_page;

  $results = array();
  $results['page'] = "articles";
  $data = Category::getList($start_from,$record_per_page);
  $results['categories'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "Article Categories";

  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "categoryNotFound" ) $results['errorMessage'] = "Error: Category not found.";
    if ( $_GET['error'] == "categoryContainsArticles" ) $results['errorMessage'] = "Error: Category contains articles. Delete the articles, or assign them to another category, before deleting this category.";
  }

  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "categoryDeleted" ) $results['statusMessage'] = "Category deleted.";
  }

  require( TEMPLATE_PATH . "/categories/listCategories.php" );
}


function newCategory() {

  $results = array();
  $results['page'] = "articles";
  $results['pageTitle'] = "New Article Category";
  $results['formAction'] = "newCategory";

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the category edit form: save the new category
    $category = new Category;
    $category->storeFormValues( $_POST );
    $category->insert();
    header( "Location: index.php?action=listCategories&status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the category list
    header( "Location: index.php?action=listCategories" );
  } else {

    // User has not posted the category edit form yet: display the form
    $results['category'] = new Category;
    require( TEMPLATE_PATH . "/categories/editCategory.php" );
  }

}


function editCategory() {

  $results = array();
    $results['page'] = "articles";
  $results['pageTitle'] = "Edit Article Category";
  $results['formAction'] = "editCategory";

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the category edit form: save the category changes

    if ( !$category = Category::getById( (int)$_POST['categoryId'] ) ) {
      header( "Location: index.php?action=listCategories&error=categoryNotFound" );
      return;
    }

    $category->storeFormValues( $_POST );
    $category->update();
    header( "Location: index.php?action=listCategories&status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the category list
    header( "Location: index.php?action=listCategories" );
  } else {

    // User has not posted the category edit form yet: display the form
    $results['category'] = Category::getById( (int)$_GET['categoryId'] );
    require( TEMPLATE_PATH . "/categories/editCategory.php" );
  }

}


function deleteCategory() {

  if ( !$category = Category::getById( (int)$_GET['categoryId'] ) ) {
    header( "Location: index.php?action=listCategories&error=categoryNotFound" );
    return;
  }

  $articles = Article::getList( 1000000, $category->id );

  if ( $articles['totalRows'] > 0 ) {
    header( "Location: index.php?action=listCategories&error=categoryContainsArticles" );
    return;
  }

  $category->delete();
  header( "Location: index.php?action=listCategories&status=categoryDeleted" );
}



// =========================== COACHES ==========================================


function listCoaches() {


  $record_per_page = COACHES_PAGE_PER_TABLE;
  if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
  $start_from = ($page-1) * $record_per_page;

  $results = array();
  $results['page'] = "teams";
  $data = Coach::getList($start_from,$record_per_page);
  $results['coaches'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "Coaches";

  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "coachNotFound" ) $results['errorMessage'] = "Error: Coach not found.";
    if ( $_GET['error'] == "coachContainsTeam" ) $results['errorMessage'] = "Error: Coach contains Team. Delete the team, or assign them to another coach, before deleting this coach.";
  }

  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "coachDeleted" ) $results['statusMessage'] = "Coach deleted.";
  }

  require( TEMPLATE_PATH . "/coaches/listCoaches.php" );
}


function newCoach() {

  $results = array();
  $results['page'] = "teams";
  $results['pageTitle'] = "New Coach";
  $results['formAction'] = "newCoach";

  if ( isset( $_POST['saveChanges'] ) ) {



    // User has posted the coach edit form: save the new coach
    $coach = new Coach;
    $coach->storeFormValues( $_POST );
    $coach->insert();
    if ( isset( $_FILES['image'] ) ) $coach->storeUploadedImage( $_FILES['image'] );
    header( "Location: index.php?action=listCoaches&status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the coach list
    header( "Location: index.php?action=listCoaches" );
  } else {

    // User has not posted the coach edit form yet: display the form
    $results['coach'] = new Coach;
    require( TEMPLATE_PATH . "/coaches/editCoach.php" );
  }

}


function editCoach() {

  $results = array();
  $results['page'] = "teams";
  $results['pageTitle'] = "Edit Coach";
  $results['formAction'] = "editCoach";

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the category edit form: save the category changes

    if ( !$coach = Coach::getById( (int)$_POST['coachId'] ) ) {
      header( "Location: index.php?action=listCoaches&error=coachNotFound" );
      return;
    }

    $coach->storeFormValues( $_POST );
        if ( isset($_POST['deleteImage']) && $_POST['deleteImage'] == "yes" ) $coach->deleteImages();
    $coach->update();
    if ( isset( $_FILES['image'] ) ) $coach->storeUploadedImage( $_FILES['image'] );

    header( "Location: index.php?action=listCoaches&status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the coach list
    header( "Location: index.php?action=listCoaches" );
  } else {

    // User has not posted the coach edit form yet: display the form
    $results['coach'] = Coach::getById( (int)$_GET['coachId'] );
    require( TEMPLATE_PATH . "/coaches/editCoach.php" );
  }

}


function deleteCoach() {

  if ( !$coach = Coach::getById( (int)$_GET['coachId'] ) ) {
    header( "Location: index.php?action=listCoaches&error=coachNotFound" );
    return;
  }

  // $teams = Team::getList( 1000000, $coach->id );
  //
  // if ( $teams['totalRows'] > 0 ) {
  //   header( "Location: index.php?action=listCoaches&error=coachContainsTeams" );
  //   return;
  // }

  $coach->deleteImages();

  $coach->delete();

  header( "Location: index.php?action=listCoaches&status=coachDeleted" );
}


?>
