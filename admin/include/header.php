<!DOCTYPE html>
<html lang="en">
  <head>
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<!--PHP function htmlspecialchars() encodes any special HTML characters,
    	such as <, >, and &, into their HTML entity equivalents (&lt;, &gt;, and &amp;) -->
   	<title><?php echo htmlspecialchars( $results['pageTitle'] )?></title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href=" ../css/signin.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
    <!-- Custom CSS -->
    <link href="../css/blog-home.css" rel="stylesheet">
      <link href="../css/adminstyle.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


  </head>

  <body>
  <div id="wrap">
     <!-- Navigation -->


    <nav class="navbar navbar-default  navbar-fixed-top navbar-admin" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a  style="color:#fff;"class="navbar-brand" href=".">Admin Dashboard</a>
            </div>

          <?php if (isset($_SESSION['username'] )): ?>
            <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar">

            <li class="dropdown  <?php if($results['page']=="articles") : echo "dropdown-active"; endif; ?> ">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Articles <span class="caret"></span></a>
              <ul class="dropdown-menu">
                      <!-- <li><a href="?action=profile">Profile</a></li> -->
                      <li ><a class="nav-dropdown" href="?action=listCategories">Categories</a></li>
                      <li ><a class="nav-dropdown" href="?action=listArticles">Articles</a></li>
              </ul>
            </li>
             <li class=""><a href="#">Games</a></li>
            <li class="dropdown  <?php if($results['page']=="teams") : echo "dropdown-active"; endif; ?> ">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Teams <span class="caret"></span></a>
              <ul class="dropdown-menu">
                      <!-- <li><a href="?action=profile">Profile</a></li> -->
                      <li ><a class="nav-dropdown" href=".">Teams</a></li>
                      <li ><a class="nav-dropdown" href=".">Players</a></li>
                      <li ><a class="nav-dropdown" href="?action=listCoaches">Coaches</a></li>
                      <li ><a class="nav-dropdown" href=".">Positions</a></li>
                      <li ><a class="nav-dropdown" href=".">Barangays</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Schedules <span class="caret"></span></a>
              <ul class="dropdown-menu">
                      <!-- <li><a href="?action=profile">Profile</a></li> -->
                      <li ><a class="nav-dropdown" href=".">Season</a></li>
                      <li ><a class="nav-dropdown" href=".">Schedules</a></li>
              </ul>
            </li>


            </ul>

          <ul class="nav navbar-nav navbar-right">
          <li class="dropdown ">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo htmlspecialchars( $_SESSION['username']) ?> <span class="caret"></span></a>
            <ul class="dropdown-menu">
                    <!-- <li><a href="?action=profile">Profile</a></li> -->
                    <li ><a class="nav-dropdown" href="?action=logout">Sign Out</a></li>
            </ul>
          </li>
          </ul>
          </div><!-- /.navbar-collapse -->

          <?php endif; ?>

        </div>
        <!-- /.container -->
    </nav>
    <!-- Page Content -->
    <div  class="container">

        <div style="padding-top:70px;" class="row">
